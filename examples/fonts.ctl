Section: fonts
Priority: optional
#Homepage: https://some.where
Standards-Version: 4.5.0

Package: fonts-arimo
Version: 1.0
# Maintainer: Your Name <yourname@example.com>
Architecture: all
Multi-Arch: foreign
# Copyright: <copyright file; defaults to GPL2>
# Changelog: <changelog file; defaults to a generic changelog>
# Readme: <README.Debian file; defaults to a generic one>
Files: /root/.fonts/Arimo[wght].ttf /usr/share/fonts/truetype/arimo/
 /root/.fonts/Arimo-Italic[wght].ttf /usr/share/fonts/truetype/arimo/
Description: some description
 long description and info
 .
 second paragraph
